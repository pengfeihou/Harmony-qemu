/*
 * This file is part of the µOS++ distribution.
 *   (https://github.com/micro-os-plus)
 * Copyright (c) 2014 Liviu Ionescu.
 *
 * Permission is hereby granted, free of charge, to any person
 * obtaining a copy of this software and associated documentation
 * files (the "Software"), to deal in the Software without
 * restriction, including without limitation the rights to use,
 * copy, modify, merge, publish, distribute, sublicense, and/or
 * sell copies of the Software, and to permit persons to whom
 * the Software is furnished to do so, subject to the following
 * conditions:
 *
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
 * OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
 * HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
 * WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
 * OTHER DEALINGS IN THE SOFTWARE.
 */

// ----------------------------------------------------------------------------

#include <stdio.h>
#include <stdlib.h>
#include "diag/trace.h"
#include "los_tick.h"
#include "los_task.h"
#include "los_config.h"
#include "los_interrupt.h"
#include "los_debug.h"
#include "los_compiler.h"
#include "target_config.h"
#include "stm32f429i_discovery.h"
#include "stm32f4xx_hal.h"

UINT8 g_memStart[OS_SYS_MEM_SIZE];
// ----------------------------------------------------------------------------
//
// Semihosting STM32F4 empty sample (trace via DEBUG).
//
// Trace support is enabled by adding the TRACE macro definition.
// By default the trace messages are forwarded to the DEBUG output,
// but can be rerouted to any device or completely suppressed, by
// changing the definitions required in system/src/diag/trace-impl.c
// (currently OS_USE_TRACE_ITM, OS_USE_TRACE_SEMIHOSTING_DEBUG/_STDOUT).
//

// ----- main() ---------------------------------------------------------------

// Sample pragmas to cope with warnings. Please note the related line at
// the end of this function, used to pop the compiler diagnostics status.
#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wunused-parameter"
#pragma GCC diagnostic ignored "-Wmissing-declarations"
#pragma GCC diagnostic ignored "-Wreturn-type"

VOID taskSampleEntry2(VOID)
{
    while(1) {
      HAL_GPIO_TogglePin(LED4_GPIO_PORT, LED4_PIN);
      LOS_TaskDelay(500);
      printf("taskSampleEntry2 running...\n");
    }
}


VOID taskSampleEntry1(VOID)
{
    while(1) {
      HAL_GPIO_TogglePin(LED3_GPIO_PORT, LED3_PIN);
      LOS_TaskDelay(1000);
      printf("taskSampleEntry1 running...\n");
    }

}

UINT32 taskSample(VOID)
{
    UINT32  uwRet;
    UINT32  taskID1,taskID2;
    TSK_INIT_PARAM_S stTask1={0};
    stTask1.pfnTaskEntry = (TSK_ENTRY_FUNC)taskSampleEntry1;
    stTask1.uwStackSize  = 0X1000;
    stTask1.pcName       = "taskSampleEntry1";
    stTask1.usTaskPrio   = 6;
    uwRet = LOS_TaskCreate(&taskID1, &stTask1);
    if (uwRet != LOS_OK) {
    	printf("task1 create failed\n");
    }

    stTask1.pfnTaskEntry = (TSK_ENTRY_FUNC)taskSampleEntry2;
    stTask1.uwStackSize  = 0X1000;
    stTask1.pcName       = "taskSampleEntry2";
    stTask1.usTaskPrio   = 7;
    uwRet = LOS_TaskCreate(&taskID2, &stTask1);
    if (uwRet != LOS_OK) {
    	printf("task2 create failed\n");
    }
    return LOS_OK;
}

LITE_OS_SEC_TEXT_INIT int main(int argc, char* argv[])
{
	trace_printf("System clock:%u HZ\n",SystemCoreClock);

	unsigned int ret;
	HAL_Init();
	//SystemClock_Config();
	BSP_LED_Init(LED3);
	BSP_LED_Init(LED4);

	HAL_GPIO_TogglePin(LED3_GPIO_PORT, LED3_PIN);
	HAL_GPIO_TogglePin(LED4_GPIO_PORT, LED4_PIN);


	ret = LOS_KernelInit();
	taskSample();
	if (ret == LOS_OK)
	{
		LOS_Start();
	}
	while(1)
	{
		__asm volatile("wfi");
	}
	return 0;
}

#pragma GCC diagnostic pop

// ----------------------------------------------------------------------------
