################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
../kernel_liteos_m/kal/cmsis/cmsis_liteos.c \
../kernel_liteos_m/kal/cmsis/cmsis_liteos2.c 

OBJS += \
./kernel_liteos_m/kal/cmsis/cmsis_liteos.o \
./kernel_liteos_m/kal/cmsis/cmsis_liteos2.o 

C_DEPS += \
./kernel_liteos_m/kal/cmsis/cmsis_liteos.d \
./kernel_liteos_m/kal/cmsis/cmsis_liteos2.d 


# Each subdirectory must supply rules for building sources it contributes
kernel_liteos_m/kal/cmsis/%.o: ../kernel_liteos_m/kal/cmsis/%.c
	@echo 'Building file: $<'
	@echo 'Invoking: GNU Arm Cross C Compiler'
	arm-none-eabi-gcc -mcpu=cortex-m4 -mthumb -mfloat-abi=soft -Og -fmessage-length=0 -fsigned-char -ffunction-sections -fdata-sections -fno-move-loop-invariants -Wall -Wextra  -g3 -DDEBUG -DUSE_FULL_ASSERT -DOS_USE_SEMIHOSTING -DTRACE -DOS_USE_TRACE_SEMIHOSTING_DEBUG -DSTM32F429xx -DUSE_HAL_DRIVER -DHSE_VALUE=8000000 -I"../include" -I"../system/include" -I"../system/include/cmsis" -I"../system/include/stm32f4-hal" -I../kernel_liteos_m/kernel/arch/arm/cortex-m4/gcc -I../kernel_liteos_m/kernel/include -I../kernel_liteos_m/kernel/arch/include -I../kernel_liteos_m/components/bounds_checking_function/include -I../kernel_liteos_m/utils -std=gnu11 -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@)" -c -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


