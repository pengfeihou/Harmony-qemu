################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
../kernel_liteos_m/kal/posix/src/errno.c \
../kernel_liteos_m/kal/posix/src/file.c \
../kernel_liteos_m/kal/posix/src/libc.c \
../kernel_liteos_m/kal/posix/src/malloc.c \
../kernel_liteos_m/kal/posix/src/pthread.c \
../kernel_liteos_m/kal/posix/src/pthread_attr.c \
../kernel_liteos_m/kal/posix/src/pthread_mutex.c \
../kernel_liteos_m/kal/posix/src/semaphore.c \
../kernel_liteos_m/kal/posix/src/time.c 

OBJS += \
./kernel_liteos_m/kal/posix/src/errno.o \
./kernel_liteos_m/kal/posix/src/file.o \
./kernel_liteos_m/kal/posix/src/libc.o \
./kernel_liteos_m/kal/posix/src/malloc.o \
./kernel_liteos_m/kal/posix/src/pthread.o \
./kernel_liteos_m/kal/posix/src/pthread_attr.o \
./kernel_liteos_m/kal/posix/src/pthread_mutex.o \
./kernel_liteos_m/kal/posix/src/semaphore.o \
./kernel_liteos_m/kal/posix/src/time.o 

C_DEPS += \
./kernel_liteos_m/kal/posix/src/errno.d \
./kernel_liteos_m/kal/posix/src/file.d \
./kernel_liteos_m/kal/posix/src/libc.d \
./kernel_liteos_m/kal/posix/src/malloc.d \
./kernel_liteos_m/kal/posix/src/pthread.d \
./kernel_liteos_m/kal/posix/src/pthread_attr.d \
./kernel_liteos_m/kal/posix/src/pthread_mutex.d \
./kernel_liteos_m/kal/posix/src/semaphore.d \
./kernel_liteos_m/kal/posix/src/time.d 


# Each subdirectory must supply rules for building sources it contributes
kernel_liteos_m/kal/posix/src/%.o: ../kernel_liteos_m/kal/posix/src/%.c
	@echo 'Building file: $<'
	@echo 'Invoking: GNU Arm Cross C Compiler'
	arm-none-eabi-gcc -mcpu=cortex-m4 -mthumb -mfloat-abi=soft -Og -fmessage-length=0 -fsigned-char -ffunction-sections -fdata-sections -fno-move-loop-invariants -Wall -Wextra  -g3 -DDEBUG -DUSE_FULL_ASSERT -DOS_USE_SEMIHOSTING -DTRACE -DOS_USE_TRACE_SEMIHOSTING_DEBUG -DSTM32F429xx -DUSE_HAL_DRIVER -DHSE_VALUE=8000000 -I"../include" -I"../system/include" -I"../system/include/cmsis" -I"../system/include/stm32f4-hal" -I../kernel_liteos_m/kernel/arch/arm/cortex-m4/gcc -I../kernel_liteos_m/kernel/include -I../kernel_liteos_m/kernel/arch/include -I../kernel_liteos_m/components/bounds_checking_function/include -I../kernel_liteos_m/utils -std=gnu11 -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@)" -c -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


